IssueTracker::Application.routes.draw do
  root "welcome#show"
  devise_for :users
  resources :users
  resources :projects do
    resources :issues do
      member do
        put 'assign_user', to: 'issues#assign_user', as: 'assign_user'
      end
      resources :comments do
        resources :likes, only: [:create]
      end
    end
    resources :memberships
    get 'members', to: 'users#index', as: 'users'
    resources :topics do
      resources :posts
    end
  end
end
