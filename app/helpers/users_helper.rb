module UsersHelper

  def show_time time
    time.strftime("%a %m/%d/%y")
  end
end
