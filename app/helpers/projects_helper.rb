module ProjectsHelper

  def show_homepage homepage
    if project.homepage.present?
      link_to homepage, "http://#{project.homepage}"
    else
      'unknown'
    end
  end

end
