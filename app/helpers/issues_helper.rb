module IssuesHelper

  def show_assignee assignee
    assignee.present? ? assignee : 'unassigned'
  end

end
