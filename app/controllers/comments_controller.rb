class CommentsController < ApplicationController
  expose(:comment, attributes: :comment_params)
  expose(:issue)

  def create
    comment.user = current_user
    comment.issue = issue
    comment.save
    redirect_to :back
  end

  def update
    if comment.save

    else
      render :edit
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end
