class TopicsController < ApplicationController
  before_action :authenticate_user!
  before_action :deny_access, unless: :project_member?
  expose(:project)
  expose(:topic, attributes: :topic_params)
  expose(:topics)
  expose(:post) { Post.new }

  def create
    topic.user = current_user
    topic.project = project
    if topic.save
      redirect_to project_topic_path(project, topic)
    else
      render :new
    end
  end

  def update
    if topic.save
      redirect_to project_topic_path(project, topic)
    else
      render :edit
    end
  end

  private
    def topic_params
      params.require(:topic).permit(:title, posts_attributes: [:id, :body, :user_id])
    end
end
