class MembershipsController < ApplicationController
  before_action :authenticate_user!
  before_action :deny_access, unless: :project_member?
  expose(:project)
  expose(:membership, attributes: :membership_params)

  def create
    membership.project = project
    if membership.save
      redirect_to project
    else
      render :new
    end
  end

  private
    def membership_params
      params.require(:membership).permit(:project_id, :user_id)
    end
end