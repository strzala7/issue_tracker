class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :deny_access, unless: :project_member?
  expose(:topic)
  expose(:post, attributes: :post_params)
  expose(:posts)

  def create
    post.topic = topic
    post.user = current_user
    if post.save
      redirect_to project_topic_path(topic.project, topic)
    else
      redirect_to :back
    end
  end

  def update
    if post.save
      project_topic_path(topic.project, topic)
    else
      render :edit
    end
  end

  private
    def post_params
      params.require(:post).permit(:body)
    end
end
