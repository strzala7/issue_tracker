class UsersController < ApplicationController
  before_action :authenticate_user!
  expose(:project)
  expose(:user)
  expose(:users)
end
