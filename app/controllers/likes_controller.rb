class LikesController < ApplicationController
  expose(:like)
  expose(:comment)

  def create
    unless comment.liked?(current_user) || comment.user == current_user
      like = comment.likes.build(user_id: current_user.id)
      like.save 
    end
    redirect_to :back
  end

end
