class ProjectsController < ApplicationController
  before_action :authenticate_user!
  after_action :assign_member, only: [:create]
  expose(:user)
  expose(:project, attributes: :project_params)
  expose(:projects) { Project.all }

  def create
    project.user = current_user
    if project.save
      redirect_to project
    else
      render :new
    end
  end

  def update
    if project.save
      redirect_to project
    else
      render :edit
    end
  end

  def assign_member
    Membership.create(user_id: current_user.id, project_id: project.id)
  end

  protected
    def project_params
      params.require(:project).permit(:title, :description, :homepage)
    end

end
