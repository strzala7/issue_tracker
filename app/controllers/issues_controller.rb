class IssuesController < ApplicationController
  before_action :authenticate_user!
  before_action :deny_access, unless: :project_member?
  expose(:project)
  expose(:issue, attributes: :issue_params)
  expose(:issues) { project.issues }


  def create
    issue.creator = current_user
    if issue.save
      redirect_to project_issue_path(project, issue)
    else
      render :new
    end
  end

  def update
    if issue.save
      redirect_to project_issue_path(project, issue)
    else
      render :edit
    end
  end

  def assign_user
    issue = Issue.find(params[:id])
    issue.update(assignee: current_user) if current_user.technical?
    redirect_to project_issue_path(project, issue)
  end

  private
    def issue_params
      params.require(:issue).permit(:subject, :description, :priority, :type, :state)
    end
end