class User
  include Mongoid::Document
  include Mongoid::Timestamps
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :remember_created_at, type: Time
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String
  field :first_name, type: String
  field :last_name, type: String
  field :position, type: String

  has_many :projects
  has_many :memberships
  has_many :issues, inverse_of: :creator
  has_many :topics
  has_many :posts
  has_many :comments

  POSITIONS = ['developer', 'project manager', 'other']

  def to_s
    full_name.strip.empty? ? email : full_name
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def projects_member
    Project.in(id: memberships.map(&:project_id))
  end

  def issues_assigned
    Issue.in(assignee: self)
  end

  def technical?
    position == 'developer' || position == 'project manager'
  end

end
