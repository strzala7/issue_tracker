class Like

  include Mongoid::Document
  
  belongs_to :user
  belongs_to :comment

  scope :mine, ->(user) { where(user_id: user) }
end
