class Project
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :description, type: String
  field :homepage, type: String

  belongs_to :user
  has_many :issues
  has_many :memberships
  has_many :topics

  validates :title, presence: true

  def member? user
    Membership.where(user_id: user.id, project_id: self.id).present?
  end

  def members
    User.in(id: memberships.map(&:user_id))
  end

  def users_available_to_invite
    User.not_in(id: memberships.map(&:user_id))
  end

end
