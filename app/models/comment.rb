class Comment
  include Mongoid::Document
  include Mongoid::Timestamps

  field :body, type: String
  
  belongs_to :user
  belongs_to :issue
  has_many :likes

  validates :user_id, presence: true
  validates :issue_id, presence: true
  validates :body, presence: true


  def liked? user
    likes.mine(user).present?
  end

  def can_like? user
    true unless self.liked?(user) || self.user == user
  end

end
