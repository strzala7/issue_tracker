class Topic
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String

  belongs_to :user
  belongs_to :project
  has_many :posts, dependent: :destroy
  accepts_nested_attributes_for :posts

  validates :title, length: { minimum: 5, maximum: 50 }
  validates :user_id, presence: true
  validates :project_id, presence: true

end
