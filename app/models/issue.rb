class Issue
  include Mongoid::Document
  field :subject, type: String
  field :description, type: String
  field :priority, type: String
  field :type, type: String
  field :state, type: String, default: 'open'


  belongs_to :project
  belongs_to :assignee, class_name: 'User'
  belongs_to :creator, class_name: 'User', inverse_of: :issues
  has_many :comments
  

  TYPES = %w(bug enhancement)
  PRIORITIES = %w(minor normal major critical)
  STATES = %w(open fixed)

  validates :subject, presence: true
  validates :type, inclusion: { in: TYPES }
  validates :priority, inclusion: { in: PRIORITIES }
  validates :state, inclusion: { in: STATES }

  scope :open, -> { where(state: 'open') }
  scope :fixed, -> { where(state: 'fixed') }
  scope :mine, ->(user) { where(assignee: user) }
  
end
