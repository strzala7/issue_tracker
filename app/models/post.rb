class Post
  include Mongoid::Document
  include Mongoid::Timestamps

  field :body, type: String

  belongs_to :topic
  belongs_to :user

  validates :body, length: { minimum: 5, maximum: 10000 }
end
