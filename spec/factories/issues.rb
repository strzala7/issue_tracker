# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :issue do
    subject "MyString"
    description "MyText"
    priority "MyString"
    type ""
    project nil
    assignee nil
    creator nil
  end
end
